create table products
(
    productId int auto_increment,
    name      varchar(50) charset utf8 not null,
    unitPrice decimal(7, 3)            not null,
    quantity  int                      not null,
    constraint products_name_uindex
        unique (name),
    constraint products_productId_uindex
        unique (productId)
);

alter table products
    add primary key (productId);

INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (9, 'Pfizer vaccine', 14.500, 3999000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (10, 'Moderna vaccine', 19.350, 430000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (11, 'Johnson & Johnson vaccine', 8.000, 450000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (14, 'Sinopharm vaccine', 35.500, 1200000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (15, 'Astrazeneca vaccine', 1.500, 25000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (16, 'Sputnik vaccine', 8.400, 215000);