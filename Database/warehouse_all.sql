create table clients
(
    clientId     int auto_increment,
    userName     varchar(50) charset utf8  not null,
    firstName    varchar(50) charset utf8  not null,
    lastName     varchar(50) charset utf8  not null,
    address      varchar(200) charset utf8 not null,
    phoneNumber  char(10) charset utf8     not null,
    emailAddress varchar(50) charset utf8  not null,
    constraint Clients_id_uindex
        unique (clientId),
    constraint Clients_userName_uindex
        unique (userName)
);

alter table clients
    add primary key (clientId);

INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (18, 'romanianAgency', 'Liana', 'Enescu', 'Bucuresti, Strada Eminescu 25', '0743125874', 'dominic@yahoo.com');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (19, 'germanAgency', 'Markus', 'Heller', 'Berlin, Haupstrasse 123', '0440123456', 'heller@vaccine.de');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (20, 'frenchAgency', 'Paul', 'Dumas', 'Paris, Rue de Renne, 45', '0741497265', 'paul@outlook.com');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (21, 'britishAgency', 'Emily', 'Austen', 'London, MainStreet 45', '0740895462', 'emily@gmail.com');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (22, 'russianAgency', 'Aleksandr', 'Karelin', 'Moskow', '0789546852', 'aleksandr@yahoo.ru');

create table products
(
    productId int auto_increment,
    name      varchar(50) charset utf8 not null,
    unitPrice decimal(7, 3)            not null,
    quantity  int                      not null,
    constraint products_name_uindex
        unique (name),
    constraint products_productId_uindex
        unique (productId)
);

alter table products
    add primary key (productId);

INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (9, 'Pfizer vaccine', 14.500, 3999000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (10, 'Moderna vaccine', 19.350, 430000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (11, 'Johnson & Johnson vaccine', 8.000, 450000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (14, 'Sinopharm vaccine', 35.500, 1200000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (15, 'Astrazeneca vaccine', 1.500, 25000);
INSERT INTO warehouse.products (productId, name, unitPrice, quantity) VALUES (16, 'Sputnik vaccine', 8.400, 215000);

create table orders
(
    orderId   int auto_increment,
    clientId  int           null,
    productId int           null,
    quantity  int           not null,
    unitPrice decimal(7, 3) not null,
    timeStamp datetime      not null,
    constraint orders_orderId_uindex
        unique (orderId),
    constraint orders_client_fk
        foreign key (clientId) references clients (clientId)
            on delete set null,
    constraint orders_product_fk
        foreign key (productId) references products (productId)
            on delete set null
);

alter table orders
    add primary key (orderId);

INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (43, 18, 9, 5000000, 15.500, '2021-04-22 02:52:15');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (44, 19, 9, 600000, 15.500, '2021-04-22 02:53:01');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (45, 19, 10, 50000, 19.350, '2021-04-22 02:53:29');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (46, 19, 15, 90000, 1.500, '2021-04-22 02:53:37');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (47, 22, 16, 120000, 8.400, '2021-04-22 02:55:32');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (48, 22, 15, 10000, 1.500, '2021-04-22 02:55:44');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (49, 19, 16, 5000, 8.400, '2021-04-22 02:57:23');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (50, 20, 10, 5000, 19.350, '2021-04-22 02:57:31');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (51, 20, 10, 5000, 19.350, '2021-04-22 02:57:36');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (52, 18, 9, 1000000, 15.500, '2021-04-22 02:58:07');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (53, 18, 10, 10000, 19.350, '2021-04-22 02:59:19');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (54, 19, 9, 1000, 14.500, '2021-04-22 03:04:15');