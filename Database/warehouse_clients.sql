create table clients
(
    clientId     int auto_increment,
    userName     varchar(50) charset utf8  not null,
    firstName    varchar(50) charset utf8  not null,
    lastName     varchar(50) charset utf8  not null,
    address      varchar(200) charset utf8 not null,
    phoneNumber  char(10) charset utf8     not null,
    emailAddress varchar(50) charset utf8  not null,
    constraint Clients_id_uindex
        unique (clientId),
    constraint Clients_userName_uindex
        unique (userName)
);

alter table clients
    add primary key (clientId);

INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (18, 'romanianAgency', 'Liana', 'Enescu', 'Bucuresti, Strada Eminescu 25', '0743125874', 'dominic@yahoo.com');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (19, 'germanAgency', 'Markus', 'Heller', 'Berlin, Haupstrasse 123', '0440123456', 'heller@vaccine.de');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (20, 'frenchAgency', 'Paul', 'Dumas', 'Paris, Rue de Renne, 45', '0741497265', 'paul@outlook.com');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (21, 'britishAgency', 'Emily', 'Austen', 'London, MainStreet 45', '0740895462', 'emily@gmail.com');
INSERT INTO warehouse.clients (clientId, userName, firstName, lastName, address, phoneNumber, emailAddress) VALUES (22, 'russianAgency', 'Aleksandr', 'Karelin', 'Moskow', '0789546852', 'aleksandr@yahoo.ru');