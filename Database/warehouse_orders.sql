create table orders
(
    orderId   int auto_increment,
    clientId  int           null,
    productId int           null,
    quantity  int           not null,
    unitPrice decimal(7, 3) not null,
    timeStamp datetime      not null,
    constraint orders_orderId_uindex
        unique (orderId),
    constraint orders_client_fk
        foreign key (clientId) references clients (clientId)
            on delete set null,
    constraint orders_product_fk
        foreign key (productId) references products (productId)
            on delete set null
);

alter table orders
    add primary key (orderId);

INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (43, 18, 9, 5000000, 15.500, '2021-04-22 02:52:15');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (44, 19, 9, 600000, 15.500, '2021-04-22 02:53:01');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (45, 19, 10, 50000, 19.350, '2021-04-22 02:53:29');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (46, 19, 15, 90000, 1.500, '2021-04-22 02:53:37');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (47, 22, 16, 120000, 8.400, '2021-04-22 02:55:32');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (48, 22, 15, 10000, 1.500, '2021-04-22 02:55:44');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (49, 19, 16, 5000, 8.400, '2021-04-22 02:57:23');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (50, 20, 10, 5000, 19.350, '2021-04-22 02:57:31');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (51, 20, 10, 5000, 19.350, '2021-04-22 02:57:36');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (52, 18, 9, 1000000, 15.500, '2021-04-22 02:58:07');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (53, 18, 10, 10000, 19.350, '2021-04-22 02:59:19');
INSERT INTO warehouse.orders (orderId, clientId, productId, quantity, unitPrice, timeStamp) VALUES (54, 19, 9, 1000, 14.500, '2021-04-22 03:04:15');