package com.bori.dal.Exceptions;

/**
 * Exception thrown when an entity, searched based on the key, is not found in the database.
 */
public class EntityNotFoundException extends Exception {
    private final Object type;
    private final Object key;

    /**
     * Creates a new EntityNotFoundException.
     * @param type is the type of the entity that was searched
     * @param key the key of the entity that was not found.
     */
    public EntityNotFoundException(Object type,  Object key) {
        super("Entity with id " + key + " not found in database");
        this.type = type;
        this.key = key;
    }

    /**
     * Returns the type of the entity that was searched.
     * @return the type of the object.
     */
    public Object getType() {
        return type;
    }

    /**
     * Returns the key of the entity that was not found.
     * @return the key of the entity that was not found.
     */
    public Object getKey() {
        return key;
    }
}
