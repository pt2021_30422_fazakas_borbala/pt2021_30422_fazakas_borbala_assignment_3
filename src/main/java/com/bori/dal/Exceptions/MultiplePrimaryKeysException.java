package com.bori.dal.Exceptions;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Exception thrown when a DatabaseEntity has multiple fields marked as
 * {@link com.bori.dal.Annotations.Id}.
 */
public class MultiplePrimaryKeysException extends Exception {
    /** Type of the object with multiple fields. */
    private final Object type;
     /** List of fields marked as id. */
    private final List<Field> idFields;

    /**
     * Creates a new MultiplePrimaryKeyException instance.
     * @param type is the type of the database entity which multiple primary keys.
     * @param idFields is the list of fields marked as primary key.
     */
    public MultiplePrimaryKeysException(Object type, List<Field> idFields) {
        super("Multiple id fields in " + type.toString() + ": " + idFields);
        this.type = type;
        this.idFields = idFields;
    }

    /**
     * Returns the type of the object with multiple primary key fields.
     * @return the type of the object with multiple primary keys.
     */
    public Object getType() {
        return type;
    }

    /**
     * Returns the id fields of the invalid object.
     * @return the id fields of the invalid object.
     */
    public List<Field> getIdFields() {
        return idFields;
    }
}
