package com.bori.dal.Exceptions;

/**
 * Exception thrown when the header of the table for an {@link com.bori.dal.Annotations.Entity}
 * does not correspond to the entity's description.
 */
public class InvalidTableHeaderException extends Exception {
    private final Object type;

    /**
     * Creates a new InvalidTableHeaderException instance.
     * @param type is the type of the database entity whose description doesn't correspond to an
     *             existing database table.
     */
    public InvalidTableHeaderException(Object type) {
        super("Entity " + type + " doesn't correspond to a valid database table. ");
        this.type = type;
    }

    /**
     *  Returns the type of the database entity whose description doesn't correspond to an
     *  existing database table, and for which the exception was thrown.
     * @return the type of the database entity.
     */
    public Object getType() {
        return type;
    }
}
