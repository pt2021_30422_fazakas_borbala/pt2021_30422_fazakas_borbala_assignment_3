package com.bori.dal.Exceptions;

/**
 * Exception thrown when a primary key appears multiple times in a database table.
 */
public class NonUniquePrimaryKeyException extends Exception {
    /** Type of the object whose primary key is duplicated. */
    private final Object type;
    /** Key which is duplicated in the database. */
    private final Object key;

    /**
     * Creates a new NonUniquePrimaryKeyException instance.
     * @param type is the type of the database entity for which the id duplication occurred.
     * @param key is the duplicated key.
     */
    public NonUniquePrimaryKeyException(Object type,  Object key) {
        super("Multiple instances of " + type + "with key " + key + " found in the database");
        this.type = type;
        this.key = key;
    }

    /**
     * Returns the type of the database entity for which the id duplication occurred.
     * @return the type of the object for which the primary key is duplicated.
     */
    public Object getType() {
        return type;
    }

    /**
     * Returns the duplicated key.
     * @return the duplicated key.
     */
    public Object getKey() {
        return key;
    }
}
