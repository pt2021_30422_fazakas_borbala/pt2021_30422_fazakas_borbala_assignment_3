package com.bori.dal.Exceptions;

/**
 * Exception thrown when an implementation error occurs in the data access layer whose details
 * should not be sent to the upper layers. Remark that this is the only exception that is sent
 * outside the data access layer.
 */
public class UnexpectedDatabaseException extends Exception {
    /**
     * Creates a new UnexpectedDatabaseEntityException.
     * @param cause will be used as the message of the exception.
     */
    public UnexpectedDatabaseException(String cause) {
        super("An unexpected error occured in the data access layer. " + cause);
    }

    /**
     * Creates a new UnexpectedDatabaseEntityException, with a generic error message
     */
    public UnexpectedDatabaseException() {
        super("An unexpected error occured in the data access layer.");
    }
}
