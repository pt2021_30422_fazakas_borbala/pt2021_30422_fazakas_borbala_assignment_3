package com.bori.dal.Exceptions;

/**
 * Exception thrown when a DatabaseEntity has no field marked as
 * {@link com.bori.dal.Annotations.Id}.
 */
public class MissingPrimaryKeyException extends Exception {
    private final Object type;

    /**
     * Creates a new MissingPrimaryKeyException instance.
     * @param type is the type of the database entity which has no primary key.
     */
    public MissingPrimaryKeyException(Object type) {
        super("Entity " + type + " has no id field.");
        this.type = type;
    }

    /**
     * Returns the object type which has illegally has no primary key.
     * @return the object with missing primary key.
     */
    public Object getType() {
        return type;
    }
}
