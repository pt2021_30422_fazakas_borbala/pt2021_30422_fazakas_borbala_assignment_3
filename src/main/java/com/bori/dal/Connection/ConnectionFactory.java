package com.bori.dal.Connection;

import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * ConnectionFactory is responsible for creating connections to the database.
 * Remark that due to the singleton pattern, only one ConnectionFactory instance exists.
 */
public class ConnectionFactory {
    /** Driver for MySql connection. */
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    /** Url of the database to connect to. */
    private static final String DBURL = "jdbc:mysql://localhost:3306/warehouse";
    /** Credentials for the connection. */
    private static final String USER = "root";
    private static final String PASSWORD = "password";

    private static final ConnectionFactory singleInstance = new ConnectionFactory();


    public static Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    private ConnectionFactory() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the only instance of the ConnectionFactory.
     * @return the only instance of the ConnectionFactory.
     */
    public static ConnectionFactory getInstance() {
        return singleInstance;
    }

    /**
     * Creates one new connection to the database.
     * @return the single connection to the database.
     */
    public Connection getConnection() {
        try {
            return DriverManager.getConnection(DBURL, USER, PASSWORD);
            // keys are on
        } catch (SQLException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }
}
