package com.bori.dal.Entities;

/**
 * Represents an entity that is not yet saved in the database and thus has no id.
 */
public interface SavableEntity<T> {
    /**
     * Returns the actual object to be saved.
     * @return the object to save.
     */
    T getObjectToSave();
}
