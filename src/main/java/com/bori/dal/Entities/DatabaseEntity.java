package com.bori.dal.Entities;

/**
 * Represents an entity that has been saved in the database and thus has an id. Each
 * DatabaseEntity must have a parameterless constructor, and is expected to be marked with
 * {@link com.bori.dal.Annotations.Entity}.
 */
public interface DatabaseEntity<K> {
    K getId();
}
