package com.bori.dal.Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation with metadata about the database table corresponding to a
 * {@link com.bori.dal.Annotations.Entity}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table {
    /**
     * Specifies the name of the table in the database, corresponding to the marked entity.
     * @return the table name, if the data is present, otherwise an meptry string.
     */
    String name() default "";
}
