package com.bori.dal.DAO;

import com.bori.dal.Exceptions.*;
import com.bori.model.Saved.Product;

/**
 * DataAccessObject for Product entities.
 */
public class ProductDAO extends DAO<Product, Integer> {
    /**
     * Creates a new ProductDAO instance.
     * @throws UnexpectedDatabaseException if the connection to the database could not be
     * established or the database is in a corrupted or out-of-date state.
     */
    public ProductDAO() throws UnexpectedDatabaseException {
        super(Product.class);
    }
}
