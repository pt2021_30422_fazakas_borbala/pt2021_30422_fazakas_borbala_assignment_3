package com.bori.dal.DAO;

import com.bori.dal.Exceptions.*;
import com.bori.model.Saved.Client;

import java.sql.SQLException;

/**
 * DataAccessObject for Client entities.
 */
public class ClientDAO extends DAO<Client, Integer>{

    /**
     * Creates a new ClientDAO instance.
     * @throws UnexpectedDatabaseException if the connection to the database could not be
     * established or the database is in a corrupted or out-of-date state.
     */
    public ClientDAO() throws UnexpectedDatabaseException {
        super(Client.class);
    }
}
