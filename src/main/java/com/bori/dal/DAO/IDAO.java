package com.bori.dal.DAO;

import com.bori.dal.Entities.DatabaseEntity;
import com.bori.dal.Entities.SavableEntity;
import com.bori.dal.Exceptions.*;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * IDAO provides a generic interface for database access.
 * @param <T> specifies the type of the entity which is inserted, updated, retrieved or deleted
 *           from the database.
 */
public interface IDAO<T extends DatabaseEntity<K>, K> {
    /**
     * Returns all tuples from the database which are instances of T.
     * @return a list containing all instances of T from the database.
     * @throws UnexpectedDatabaseException if any error occurs.
     */
    List<T> findAll() throws UnexpectedDatabaseException;

    /**
     * Returns at most one tuple from the database which is the instance of T and have the primary
     * key equal to the key parameter.
     * @param key is the searched primary key.
     * @return the single instance of T with the primary key equal to key, if it exists, wrapped
     * in an optional. If it doesn't exist, an empty optional is returned.
     * @throws UnexpectedDatabaseException if any error occurs.
     */
    Optional<T> findById(K key) throws UnexpectedDatabaseException;

    /**
     * Inserts into the database a new tuple corresponding to the entity.
     * @param entity is the entity to save.
     * @return the inserted entity. The optional being empty marks an error.
     * @throws UnexpectedDatabaseException if any error occurs.
     */
    Optional<T> insert(SavableEntity<T> entity) throws UnexpectedDatabaseException;

    /**
     * Removes from the database all instances of T with the primary key specified.
     * @param key is the value of the primary key with which the entities are deleted.
     * @throws UnexpectedDatabaseException if any error occurs.
     */
    void delete(K key) throws UnexpectedDatabaseException;

    /**
     * Removes from the database all instances of T with the primary key qual to key.
     * @param entity is the editit to edit.
     * @return the edited entity. The optional being empty marks an error.
     * @throws UnexpectedDatabaseException if any error occurs.
     */
    Optional<T> edit(T entity) throws UnexpectedDatabaseException;

    /**
     * Returns all instances of T from the database which are instances of T and have the value of
     * field equal to value.
     * @param field is the field whose value is evaluated.
     * @param value is the searched value.
     * @return a list containing all instances of T which fulfill the criteria.
     * @throws UnexpectedDatabaseException if any error occurs.
     */
    List<T> findByField(Field field, Object value) throws UnexpectedDatabaseException;

}
