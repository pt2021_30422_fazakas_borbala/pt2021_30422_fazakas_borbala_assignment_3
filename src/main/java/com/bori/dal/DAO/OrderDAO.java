package com.bori.dal.DAO;

import com.bori.dal.Exceptions.*;
import com.bori.model.Saved.Order;

import java.sql.SQLException;

/**
 * DataAccessObject for Order entities.
 */
public class OrderDAO extends DAO<Order, Integer>{
    /**
     * Creates a new OrderDAO instance.
     * @throws UnexpectedDatabaseException if the connection to the database could not be
     * established or the database is in a corrupted or out-of-date state.
     */
    public OrderDAO() throws UnexpectedDatabaseException {
        super(Order.class);
    }
}
