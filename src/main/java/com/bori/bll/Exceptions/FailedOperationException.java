package com.bori.bll.Exceptions;

/**
 * Exception thrown by the business logic layer in case an operation unexpectedly fails, not
 * because of any mistake made by the user.
 */
public class FailedOperationException extends Exception {
    public FailedOperationException() {
        super("This operation failed. Please retry");
    }
}
