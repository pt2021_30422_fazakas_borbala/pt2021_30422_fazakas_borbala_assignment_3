package com.bori.bll.Exceptions;

import com.bori.bll.Validators.DatabaseEntityValidationResult;

/**
 * Exception thrown by the business logic layer in case an operation fails dues to invalid user
 * input. In this case, the message of the exception shows the cause of the error, and it is
 * indented to inform the user about what needs to be corrected.
 */
public class InvalidDatabaseEntityException extends Exception {
    public InvalidDatabaseEntityException(DatabaseEntityValidationResult result) {
        super(result.getErrorMessage());
    }
}
