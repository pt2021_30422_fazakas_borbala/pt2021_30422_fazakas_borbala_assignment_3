package com.bori.bll.Validators;

import com.bori.dal.DAO.IDAO;
import com.bori.dal.Entities.DatabaseEntity;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.dal.Util.DatabaseEntityUtil;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Defines an abstract implementation of DatabaseEntityValidator which handles validation common
 * to all DatabaseEntity types.
 * @param <T> is the type of the validated object.
 * @param <K> is the type of T's id.
 */
public abstract class AbstractDatabaseEntityValidator<T extends DatabaseEntity<K>, K> implements DatabaseEntityValidator<T> {

    private final Class<T> type;
    private final IDAO<T, K> DAO;
    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Construct a validator of a certain type.
     * @param type specifies the type of T.
     * @param DAO specifies the data access object used for validation.
     */
    protected AbstractDatabaseEntityValidator(Class<T> type, IDAO<T, K> DAO) {
        this.type = type;
        this.DAO = DAO;
    }

    /**
     * Searches for and returns the field of the entity. that are null although are marked
     * not nullable.
     * @param entity is the validated object.
     * @return a list of the null-valued fields which are not allowed to be null.
     */
    protected List<Field> getInvalidNullableFields(T entity) {
        List<Field> fieldList = DatabaseEntityUtil.getColumnFields(type);
        return fieldList.stream().filter(DatabaseEntityUtil::isNullableColumnField)
                                .filter(field -> {
                                        //search non-nullable fields with nullable value
                                        field.setAccessible(true);
                                        try {
                                            return field.get(entity) == null;
                                        } catch (IllegalAccessException e) {
                                            LOGGER.error(Throwables.getStackTraceAsString(e));
                                            e.printStackTrace();
                                            return true;
                                        }
                                    }).collect(Collectors.toList());
    }

    /**
     * Searches for and returns the field of the entity. that are not unique (considering
     * other application data) despite being marked as unique.
     * @param entity is the validated object.
     * @return a list of the non-unique fields which are requested to be unique.
     */
    protected List<Field> getInvalidUniqueFields(T entity) {
        List<Field> uniqueFields =
                DatabaseEntityUtil.getColumnFields(type).stream()
                        .filter(DatabaseEntityUtil::isUniqueColumnField)
                        .collect(Collectors.toList());
        List<Field> invalidUniqueFieldsList = new ArrayList<>();
        for (Field field : uniqueFields) {
            field.setAccessible(true);
            List<T> matchingEntities = null;
            try {
                matchingEntities = DAO.findByField(field, field.get(entity));
                if (matchingEntities == null || matchingEntities.size() > 1) {
                    invalidUniqueFieldsList.add(field);
                }
                if (matchingEntities != null && matchingEntities.size() == 1 &&
                        !matchingEntities.get(0).getId().equals(entity.getId())) {
                    invalidUniqueFieldsList.add(field);
                }
            } catch (UnexpectedDatabaseException | IllegalAccessException e) {
                LOGGER.error(Throwables.getStackTraceAsString(e));
                e.printStackTrace();
            }
        }
        return invalidUniqueFieldsList;
    }
}
