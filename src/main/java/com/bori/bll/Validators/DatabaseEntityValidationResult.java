package com.bori.bll.Validators;

import com.bori.dal.Entities.DatabaseEntity;

/**
 * DatabaseEntityValidationResult specifies an interface for validation results.
 * @param <T> is the type of the validated entity.
 */
public interface DatabaseEntityValidationResult<T extends DatabaseEntity> {
    /**
     * Shows whether the validated entity was valid.
     * @return true if and only if the validated entity is valid.
     */
    boolean isValid();

    /**
     * Specifiec a message explaining the reason the validated entity was considered incorrect.
     * @return the error message.
     */
    String getErrorMessage();
}
