package com.bori.bll.Validators;

import com.bori.dal.Entities.DatabaseEntity;

/**
 * FieldValidator specifies a generic interface for validating eexactly one field of a T instance.
 * @param <T> represents the type of the validated entity.
 */
public interface FieldValidator<T extends DatabaseEntity> {
    /**
     * Validates a certain field of entity..
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    DatabaseEntityValidationResult<T> validField(T entity);
}
