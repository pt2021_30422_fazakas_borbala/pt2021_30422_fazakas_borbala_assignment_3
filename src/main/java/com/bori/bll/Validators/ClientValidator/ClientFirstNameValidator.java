package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Client;

/**
 * ClientFirstNameValidator is responsible for validating the first name field of a client.
 */
public class ClientFirstNameValidator implements FieldValidator<Client> {
    /** Minimum length of first name string. */
    public static int MIN_LENGTH = 1;
    /** Maximum length of first name string. */
    public static int MAX_LENGTH = 50;

    /**
     * Validates the firstName field of the entity..
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Client> validField(Client entity) {
        if (entity.getFirstName().length() < MIN_LENGTH) {
            return ClientEntityValidationResult.TOO_SHORT_FIRST_NAME;
        }
        if (entity.getFirstName().length() > MAX_LENGTH) {
            return ClientEntityValidationResult.TOO_LONG_FIRST_NAME;
        }
        return ClientEntityValidationResult.VALID;
    }
}
