package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Client;

/**
 * ClientUsernameValidator is responsible for validating the phone number field of a client.
 */
public class ClientUsernameValidator implements FieldValidator<Client> {
    /** Minimum length of username string. */
    public static int MIN_LENGTH = 1;
    /** Maximum length of username string. */
    public static int MAX_LENGTH = 50;

    /**
     * Validates the username field of the entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Client> validField(Client entity) {
        if (entity.getUserName().length() < MIN_LENGTH) {
            return ClientEntityValidationResult.TOO_SHORT_USERNAME;
        }
        if (entity.getUserName().length() > MAX_LENGTH) {
            return ClientEntityValidationResult.TOO_LONG_USERNAME;
        }
        return ClientEntityValidationResult.VALID;
    }
}
