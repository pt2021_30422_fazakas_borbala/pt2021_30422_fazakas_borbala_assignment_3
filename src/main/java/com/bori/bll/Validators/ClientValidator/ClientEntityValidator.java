package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.AbstractDatabaseEntityValidator;
import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.dal.DAO.ClientDAO;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Client;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * DatabaseEntityValidator is intended to verify that the data of a Client instance is valid
 * and can be saved.
 */
public class ClientEntityValidator extends AbstractDatabaseEntityValidator<Client, Integer> {

    /** Validators applied to different fields of a client. */
    private final List<FieldValidator<Client>> fieldValidators = List.of(
            new ClientUsernameValidator(),
            new ClientFirstNameValidator(),
            new ClientLastNameValidator(),
            new ClientAddressValidator(),
            new ClientPhoneNumberValidator(),
            new ClientEmailAddressValidator()
    );

    /**
     * Creates a new ClientEntityValidator.
     * @throws UnexpectedDatabaseException if the Validtaor could not be constructed because the
     * connection with the database could not be established.
     */
    public ClientEntityValidator() throws UnexpectedDatabaseException {
        super(Client.class, new ClientDAO());
    }

    /** {@inheritDoc} */
    @Override
    public DatabaseEntityValidationResult<Client> validate(Client entity) {
        // check null values for non-nullable fields
        List<Field> invalidNullableFields = getInvalidNullableFields(entity);
        if (invalidNullableFields.size() > 0) {
            return ClientEntityValidationResult.getMissingDataErrorType(invalidNullableFields.get(0));
        }
        // check duplicate values for unique fields
        List<Field> invalidUniqueFields = getInvalidUniqueFields(entity);
        if (invalidUniqueFields.size() > 0) {
            return ClientEntityValidationResult.getDuplicateDataErrorType(invalidUniqueFields.get(0));
        }
        // check field validators
        for (FieldValidator<Client> fieldValidator : fieldValidators) {
            DatabaseEntityValidationResult<Client> res = fieldValidator.validField(entity);
            if (!res.isValid()) {
                return res;
            }
        }
        return ClientEntityValidationResult.VALID;
    }
}
