package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Client;

/**
 * ClientAddressValidator is responsible for validating the address field of a client.
 */
public class ClientAddressValidator implements FieldValidator<Client> {
    /** Minimum length of client string. */
    public static int MIN_LENGTH = 1;
    /** Maximum length of client string. */
    public static int MAX_LENGTH = 200;

    /**
     * Validates the address field of the entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Client> validField(Client entity) {
        if (entity.getAddress().length() < MIN_LENGTH) {
            return ClientEntityValidationResult.TOO_SHORT_ADDRESS;
        }
        if (entity.getAddress().length() > MAX_LENGTH) {
            return ClientEntityValidationResult.TOO_LONG_ADDRESS;
        }
        return ClientEntityValidationResult.VALID;
    }
}
