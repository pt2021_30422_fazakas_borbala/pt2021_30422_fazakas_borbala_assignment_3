package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.model.Saved.Client;

import java.lang.reflect.Field;

/**
 * Specifies the possible outcomes of validating a Client entity.
 */
public enum ClientEntityValidationResult implements DatabaseEntityValidationResult<Client> {
    VALID(true, ""),
    DUPLICATE_DATA(false, "Some data is duplicated. Please retry or contact an administrator."),
    DUPLICATE_USERNAME(false, "This username already exists, please choose another one!"),
    MISSING_DATA(false, "Some data is missing. Please retry or contact an administrator."),
    MISSING_USERNAME(false, "Missing username. Please provide a username."),
    MISSING_FIRSTNAME(false, "Missing first name. Please provide a first name."),
    MISSING_LASTNAME(false, "Missing last name. Please provide a last name."),
    MISSING_ADDRESS(false, "Missing address. Please provide an address."),
    MISSING_PHONE_NUMBER(false, "Missing phone number. Please provide a phone number."),
    MISSING_EMAIL_ADDRESS(false, "Missing email address. Please provide an email address."),
    INVALID_EMAIL_FORMAT(false, "Please provide a valid email address"),
    TOO_LONG_EMAIL(false,
            "Please provide an email with maximal length of " + ClientEmailAddressValidator.MAX_LENGTH),
    INVALID_PHONE_NUMBER_FORMAT(false, "Please provide a valid phone number (10 digits)"),
    TOO_SHORT_USERNAME(false,
            "Please provide a username with minimal length of " + ClientUsernameValidator.MIN_LENGTH),
    TOO_LONG_USERNAME(false,
            "Please provide a username with maximal length of " + ClientUsernameValidator.MAX_LENGTH),
    TOO_SHORT_FIRST_NAME(false,
            "Please provide a first name with minimal length of " + ClientFirstNameValidator.MIN_LENGTH),
    TOO_LONG_FIRST_NAME(false,
            "Please provide a first name with maximal length of " + ClientFirstNameValidator.MAX_LENGTH),
    TOO_SHORT_LAST_NAME(false,
            "Please provide a last name with minimal length of " + ClientLastNameValidator.MIN_LENGTH),
    TOO_LONG_LAST_NAME(false,
            "Please provide a last name with maximal length of " + ClientFirstNameValidator.MAX_LENGTH),
    TOO_SHORT_ADDRESS(false, "Please provide an address with minimal length of " + ClientAddressValidator.MIN_LENGTH),
    TOO_LONG_ADDRESS(false,
            "Please provide an address with maximal length of " + ClientAddressValidator.MAX_LENGTH);

    private final boolean valid;
    private final String errorMessage;


    ClientEntityValidationResult(boolean valid, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid() {
        return valid;
    }

    /** {@inheritDoc} */
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Returns the validation result corresponding to a missing field.
     * @param field represents the missing field.
     * @return the validation result corresponding to the missing field type.
     */
    public static ClientEntityValidationResult getMissingDataErrorType(Field field) {
        switch (field.getName()) {
            case "userName": return MISSING_USERNAME;
            case "firstName": return MISSING_FIRSTNAME;
            case "lastName": return MISSING_LASTNAME;
            case "address": return MISSING_ADDRESS;
            case "phoneNumber": return MISSING_PHONE_NUMBER;
            case "emailAddress": return MISSING_EMAIL_ADDRESS;
            default:
                // Returned if the missing field could not be identified.
                return MISSING_DATA;
        }
    }

    /**
     * Returns the validation result corresponding to the duplicated data field.
     * @param field represents the duplicated field.
     * @return the validation result corresponding to the duplicated field.
     */
    public static ClientEntityValidationResult getDuplicateDataErrorType(Field field) {
        if ("userName".equals(field.getName())) {
            return DUPLICATE_USERNAME;
        }
        return DUPLICATE_DATA;
    }
}
