package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * CLientEmailAddressValidator is responsible for validating the email address field of a client.
 */
public class ClientEmailAddressValidator implements FieldValidator<Client> {
    /** Maximum length of email string. */
    public static final int MAX_LENGTH = 50;
    private static final String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
    private static final Pattern pattern= Pattern.compile(regex);

    /**
     * Validates the email field of the entity..
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Client> validField(Client entity) {
        if (entity.getEmailAddress().length() > MAX_LENGTH) {
            return ClientEntityValidationResult.TOO_LONG_EMAIL;
        }
        Matcher matcher = pattern.matcher(entity.getEmailAddress());
        if (!matcher.matches()) {
            return ClientEntityValidationResult.INVALID_EMAIL_FORMAT;
        }
        return ClientEntityValidationResult.VALID;
    }
}
