package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ClientPhoneNumberValidator is responsible for validating the phone number field of a client.
 */
public class ClientPhoneNumberValidator implements FieldValidator<Client> {
    public static final String regex = "[\\d]{10}";
    private static final Pattern pattern= Pattern.compile(regex);

    /**
     * Validates the phoneNumber field of entity..
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Client> validField(Client entity) {
        Matcher matcher = pattern.matcher(entity.getPhoneNumber());
        if (!matcher.matches()) {
            return ClientEntityValidationResult.INVALID_PHONE_NUMBER_FORMAT;
        }
        return ClientEntityValidationResult.VALID;
    }
}
