package com.bori.bll.Validators.ClientValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Client;

/**
 * ClientLastNameValidator is responsible for validating the last name field of a client.
 */
public class ClientLastNameValidator implements FieldValidator<Client> {
    /** Minimum length of last name string. */
    public static int MIN_LENGTH = 1;
    /** Maximum length of last name string. */
    public static int MAX_LENGTH = 50;

    /**
     * Validates the lastName field of the enitiy..
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Client> validField(Client entity) {
        if (entity.getLastName().length() < MIN_LENGTH) {
            return ClientEntityValidationResult.TOO_SHORT_LAST_NAME;
        }
        if (entity.getLastName().length() > MAX_LENGTH) {
            return ClientEntityValidationResult.TOO_LONG_LAST_NAME;
        }
        return ClientEntityValidationResult.VALID;
    }
}
