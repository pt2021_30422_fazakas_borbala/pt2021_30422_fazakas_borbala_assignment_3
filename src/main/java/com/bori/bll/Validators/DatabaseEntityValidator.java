package com.bori.bll.Validators;

import com.bori.dal.Entities.DatabaseEntity;

/**
 * DatabaseEntityValidator is intended to verify that the data of a T instance is valid
 * and can be saved.
 * @param <T> is the type of the entity to validate.
 */
public interface DatabaseEntityValidator<T extends DatabaseEntity> {

    /**
     * Validates the data of the entity..
     * @param entity is the validated object.
     * @return the result of the validation.
     */
    DatabaseEntityValidationResult<T> validate(T entity);
}
