package com.bori.bll.Validators.ProductValidator;

import com.bori.bll.Validators.AbstractDatabaseEntityValidator;
import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.dal.DAO.ProductDAO;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Product;

import java.lang.reflect.Field;
import java.util.List;

/**
 * DatabaseEntityValidator is intended to verify that the data of a Product instance is valid
 * and can be saved.
 */
public class ProductEntityValidator extends AbstractDatabaseEntityValidator<Product, Integer> {

    /** Validators applied to different fields of a client. */
    private final List<FieldValidator<Product>> fieldValidators = List.of(
            new ProductNameValidator(),
            new ProductUnitPriceValidator(),
            new ProductQuantityValidator()
    );

    /**
     * Creates a new ProductEntityValidator.
     * @throws UnexpectedDatabaseException if the Validtaor could not be constructed because the
     * connection with the database could not be established.
     */
    public ProductEntityValidator() throws UnexpectedDatabaseException {
        super(Product.class, new ProductDAO());
    }

    /** {@inheritDoc} */
    @Override
    public DatabaseEntityValidationResult<Product> validate(Product entity) {
        // check null values for non-nullable fields
        List<Field> invalidNullableFields = getInvalidNullableFields(entity);
        if (invalidNullableFields.size() > 0) {
            return ProductEntityValidationResult.getMissingDataErrorType(invalidNullableFields.get(0));
        }
        // check duplicate values for unique fields
        List<Field> invalidUniqueFields = getInvalidUniqueFields(entity);
        if (invalidUniqueFields.size() > 0) {
            return ProductEntityValidationResult.getDuplicateDataErrorType(invalidUniqueFields.get(0));
        }
        // check field validators
        for (FieldValidator<Product> fieldValidator : fieldValidators) {
            DatabaseEntityValidationResult<Product> res = fieldValidator.validField(entity);
            if (!res.isValid()) {
                return res;
            }
        }
        return ProductEntityValidationResult.VALID;
    }
}
