package com.bori.bll.Validators.ProductValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.model.Saved.Product;

import java.lang.reflect.Field;

/**
 * Specifies the possible outcomes of validating an Order entity.
 */
public enum ProductEntityValidationResult implements DatabaseEntityValidationResult<Product> {
    VALID(true, ""),
    DUPLICATE_DATA(false, "Some data is duplicated. Please retry or contact an administrator."),
    DUPLICATE_NAME(false, "This product name already exists, please choose another one!"),
    MISSING_DATA(false, "Some data is missing. Please retry or contact an administrator."),
    MISSING_NAME(false, "Missing product name. Please provide a product name."),
    MISSING_QUANTITY(false, "Missing quantity. Please provide a quantity."),
    MISSING_UNIT_PRICE(false, "Missing unit price. Please provide a unit price."),
    TOO_SHORT_NAME(false,
            "Please provide a product name with minimal length of " + ProductNameValidator.MIN_LENGTH),
    TOO_LONG_NAME(false,
            "Please provide a product name with maximal length of " + ProductNameValidator.MAX_LENGTH),
    TOO_LOW_QUANTITY(false,
            "The quantity of a product must be at least " + ProductQuantityValidator.MIN_VALUE),
    TOO_LOW_UNIT_PRICE(false,
            "The unit price of a product must be at least " + ProductUnitPriceValidator.MIN_VALUE),
    TOO_HIGH_UNIT_PRICE(false, "The unit price of a product must be at most " + ProductUnitPriceValidator.MAX_VALUE);

    private final boolean valid;
    private final String errorMessage;

    ProductEntityValidationResult(boolean valid, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid() {
        return valid;
    }

    /** {@inheritDoc} */
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Returns the validation result corresponding to a missing field.
     * @param field represents the missing field.
     * @return the validation result corresponding to the missing field type.
     */
    public static ProductEntityValidationResult getMissingDataErrorType(Field field) {
        switch (field.getName()) {
            case "name": return MISSING_NAME;
            case "unitPrice": return MISSING_UNIT_PRICE;
            case "quantity": return MISSING_QUANTITY;
            default:
                // Returned if the missing field could not be identified.
                return MISSING_DATA;
        }
    }

    /**
     * Returns the validation result corresponding to the duplicated data field.
     * @param field represents the duplicated field.
     * @return the validation result corresponding to the duplicated field.
     */
    public static ProductEntityValidationResult getDuplicateDataErrorType(Field field) {
        if ("name".equals(field.getName())) {
            return DUPLICATE_NAME;
        }
        return DUPLICATE_DATA;
    }
}
