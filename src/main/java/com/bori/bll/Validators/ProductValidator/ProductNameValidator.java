package com.bori.bll.Validators.ProductValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Product;

/**
 * ProductNameValidator is responsible for validating the name field of a product.
 */
public class ProductNameValidator implements FieldValidator<Product> {
    /** Minimum length of product name. */
    public static int MIN_LENGTH = 1;
    /** Maximum length of product name. */
    public static int MAX_LENGTH = 200;

    /**
     * Validates the name field of the entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Product> validField(Product entity) {
        if (entity.getName().length() < MIN_LENGTH) {
            return ProductEntityValidationResult.TOO_SHORT_NAME;
        }
        if (entity.getName().length() > MAX_LENGTH) {
            return ProductEntityValidationResult.TOO_LONG_NAME;
        }
        return ProductEntityValidationResult.VALID;
    }
}
