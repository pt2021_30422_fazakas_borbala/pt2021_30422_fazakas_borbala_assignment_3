package com.bori.bll.Validators.ProductValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Product;

/**
 * ProductQuantityValidator is responsible for validating the quantity field of a product..
 */
public class ProductQuantityValidator implements FieldValidator<Product> {
    /** Minimum stock quantity of a product. */
    public static final int MIN_VALUE = 0;

    /**
     * Validates the quantity field of the entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Product> validField(Product entity) {
        if (entity.getQuantity() < MIN_VALUE) {
            return ProductEntityValidationResult.TOO_LOW_QUANTITY;
        }
        return ProductEntityValidationResult.VALID;
    }
}
