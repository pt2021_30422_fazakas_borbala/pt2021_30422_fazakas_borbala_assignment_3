package com.bori.bll.Validators.ProductValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.model.Saved.Product;

import java.math.BigDecimal;

/**
 * ProductUnitPriceValidator is responsible for validating the unitPrice field of a product.
 */
public class ProductUnitPriceValidator implements FieldValidator<Product> {
    /** Minimum unit price. */
    public static BigDecimal MIN_VALUE = new BigDecimal("0");
    /** Maximum unit price. */
    public static BigDecimal MAX_VALUE = new BigDecimal("9999.999");

    /**
     * Validates the unitPrice field of the entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Product> validField(Product entity) {
        if (entity.getUnitPrice().compareTo(MIN_VALUE) < 0) {
            return ProductEntityValidationResult.TOO_LOW_UNIT_PRICE;
        }
        if (entity.getUnitPrice().compareTo(MAX_VALUE) > 0) {
            return ProductEntityValidationResult.TOO_HIGH_UNIT_PRICE
;        }
        return ProductEntityValidationResult.VALID;
    }
}
