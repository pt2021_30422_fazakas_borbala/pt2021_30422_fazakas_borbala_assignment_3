package com.bori.bll.Validators.OrderValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.OrderValidator.OrderQuantityValidator;
import com.bori.bll.Validators.OrderValidator.OrderUnitPriceValidator;
import com.bori.model.Saved.Order;

import java.lang.reflect.Field;

/**
 * Specifies the possible outcomes of validating an Order entity.
 */
public enum OrderEntityValidationResult implements DatabaseEntityValidationResult<Order> {
    VALID(true, ""),
    DUPLICATE_DATA(false, "Some data is duplicated. Please retry or contact an administrator."),
    MISSING_DATA(false, "Some data is missing. Please retry or contact an administrator."),
    MISSING_QUANTITY(false, "Missing quantity. Please provide a quantity."),
    MISSING_UNIT_PRICE(false, "Missing unit price. Please provide a unit price."),
    TOO_LOW_QUANTITY(false,
            "The quantity of a product must be at least " + OrderQuantityValidator.MIN_VALUE),
    TOO_LOW_UNIT_PRICE(false,
            "The unit price of a product must be at least " + OrderUnitPriceValidator.MIN_VALUE),
    TOO_HIGH_UNIT_PRICE(false,
            "The unit price of a product must be at most " + OrderUnitPriceValidator.MAX_VALUE),
    PRODUCT_QUANTITY_EXCEEDED(false, "The maximum available quantity for this product was " +
                                      "exceeded"),
    MISSING_REFERRED_CLIENT(false, "Please choose an existing client"),
    MISSING_REFERRED_PRODUCT(false, "Please choose an existing product");

    private final boolean valid;
    private final String errorMessage;

    OrderEntityValidationResult(boolean valid, String errorMessage) {
        this.valid = valid;
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Returns the validation result corresponding to a missing field.
     * @param field represents the missing field.
     * @return the validation result corresponding to the missing field type.
     */
    public static OrderEntityValidationResult getMissingDataErrorType(Field field) {
        switch (field.getName()) {
            case "unitPrice": return MISSING_UNIT_PRICE;
            case "quantity": return MISSING_QUANTITY;
            default:
                // Returned if the missing field could not be identified.
                return MISSING_DATA;
        }
    }

    /**
     * Returns the validation result corresponding to the duplicated data field.
     * @param field represents the duplicated field.
     * @return the validation result corresponding to the duplicated field.
     */
    public static OrderEntityValidationResult getDuplicateDataErrorType(Field field) {
        return DUPLICATE_DATA;
    }
}
