package com.bori.bll.Validators.OrderValidator;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Services.ProductService;
import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.bll.Validators.OrderValidator.OrderEntityValidationResult;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Order;
import com.bori.model.Saved.Product;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * OrderQuantityValidator is responsible for validating the quantity field of an order.
 * Remark that it does not check whether the quantity is available.
 */
public class OrderQuantityValidator implements FieldValidator<Order> {
    /** Minimum ordered quantity. */
    public static final int MIN_VALUE = 0;

    /**
     * Validates the quantity field of entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Order> validField(Order entity) {
        if (entity.getQuantity() < MIN_VALUE) {
            return OrderEntityValidationResult.TOO_LOW_QUANTITY;
        }
        return OrderEntityValidationResult.VALID;
    }
}
