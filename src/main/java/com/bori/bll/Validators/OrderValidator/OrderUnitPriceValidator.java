package com.bori.bll.Validators.OrderValidator;

import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.bll.Validators.OrderValidator.OrderEntityValidationResult;
import com.bori.model.Saved.Order;

import java.math.BigDecimal;

/**
 * OrderUnitPriceValidator is responsible for validating the unit price field of an order.
 */
public class OrderUnitPriceValidator implements FieldValidator<Order> {
    /** Minimum possible unit price. */
    public static BigDecimal MIN_VALUE = new BigDecimal("0");
    /** Maximum possible unit price. */
    public static BigDecimal MAX_VALUE = new BigDecimal("9999.999");

    /**
     * Validates the quantity field of entity.
     * @param entity is the object whose field is validated.
     * @return the validation result.
     */
    @Override
    public DatabaseEntityValidationResult<Order> validField(Order entity) {
        if (entity.getUnitPrice().compareTo(MIN_VALUE) < 0) {
            return OrderEntityValidationResult.TOO_LOW_UNIT_PRICE;
        }
        if (entity.getUnitPrice().compareTo(MAX_VALUE) > 0) {
            return OrderEntityValidationResult.TOO_HIGH_UNIT_PRICE
;        }
        return OrderEntityValidationResult.VALID;
    }
}
