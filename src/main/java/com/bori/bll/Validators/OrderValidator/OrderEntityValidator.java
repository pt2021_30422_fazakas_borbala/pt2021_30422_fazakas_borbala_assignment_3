package com.bori.bll.Validators.OrderValidator;

import com.bori.bll.Validators.AbstractDatabaseEntityValidator;
import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.FieldValidator;
import com.bori.dal.DAO.OrderDAO;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Order;

import java.lang.reflect.Field;
import java.util.List;

/**
 * DatabaseEntityValidator is intended to verify that the data of an Order instance is valid
 * and can be saved.
 */
public class OrderEntityValidator extends AbstractDatabaseEntityValidator<Order, Integer> {

    /** Validators applied to different fields of a client. */
    private final List<FieldValidator<Order>> fieldValidators = List.of(
            new OrderQuantityValidator(),
            new OrderUnitPriceValidator()
    );

    /**
     * Creates a new OrderEntityValidator.
     * @throws UnexpectedDatabaseException if the Validator could not be constructed because the
     * connection with the database could not be established.
     */
    public OrderEntityValidator() throws UnexpectedDatabaseException {
        super(Order.class, new OrderDAO());
    }

    /** {@inheritDoc} */
    @Override
    public DatabaseEntityValidationResult<Order> validate(Order entity) {
        // check null values for non-nullable fields
        List<Field> invalidNullableFields = getInvalidNullableFields(entity);
        // nullables
        if (invalidNullableFields.size() > 0) {
            return OrderEntityValidationResult.getMissingDataErrorType(invalidNullableFields.get(0));
        }
        // check duplicate values for unique fields
        List<Field> invalidUniqueFields = getInvalidUniqueFields(entity);
        if (invalidUniqueFields.size() > 0) {
            return OrderEntityValidationResult.getDuplicateDataErrorType(invalidUniqueFields.get(0));
        }
        // check field validators
        for (FieldValidator<Order> fieldValidator : fieldValidators) {
            DatabaseEntityValidationResult<Order> res = fieldValidator.validField(entity);
            if (!res.isValid()) {
                return res;
            }
        }
        return OrderEntityValidationResult.VALID;
    }
}
