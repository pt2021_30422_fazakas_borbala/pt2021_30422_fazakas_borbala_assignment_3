package com.bori.bll.Services;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.bll.Util.BillGenerator;
import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.OrderValidator.OrderEntityValidationResult;
import com.bori.bll.Validators.OrderValidator.OrderEntityValidator;
import com.bori.bll.Validators.ProductValidator.ProductEntityValidator;
import com.bori.dal.DAO.OrderDAO;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Savable.SavableOrder;
import com.bori.model.Saved.Client;
import com.bori.model.Saved.Order;
import com.bori.model.Saved.Product;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Defines a service for creating new orders.
 * Implemented with the singleton pattern.
 */
public class OrderService {
    private static OrderService instance;
    private final OrderDAO orderDAO;
    private final OrderEntityValidator orderValidator;
    private final ProductEntityValidator productValidator;

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    private OrderService() throws UnexpectedDatabaseException {
        orderDAO = new OrderDAO();
        orderValidator = new OrderEntityValidator();
        productValidator = new ProductEntityValidator();
    }

    /**
     * Provides access to the single instance of OrderService.
     * @return the single instance of OrderService.
     * @throws UnexpectedDatabaseException if OrderService could not be constructed because no
     * connection to the database could be established.
     */
    public static OrderService getInstance() throws UnexpectedDatabaseException {
        if (instance == null) {
            instance = new OrderService();
        }
        return instance;
    }

    /**
     * Creates a new order entity and saves its data.
     * @param client is the client who sent the order.
     * @param product represents the ordered product.
     * @param orderedQuantity represents the quantity of the product requested by the client.
     * @throws InvalidDatabaseEntityException if the input data is invalid.
     * @throws FailedOperationException if the order creation could not be performed for
     * unexpected reasons.
     */
    public void createOrder(Client client, Product product, int orderedQuantity) throws InvalidDatabaseEntityException, FailedOperationException {
        Optional<Client> referredClient = getReferredClient(client.getId());
        if (referredClient.isEmpty()) {
            throw new InvalidDatabaseEntityException(OrderEntityValidationResult.MISSING_REFERRED_CLIENT);
        }
        Optional<Product> opOrderedProduct = getReferredProduct(product.getId());
        if (opOrderedProduct.isEmpty()) {
            throw new InvalidDatabaseEntityException(OrderEntityValidationResult.MISSING_REFERRED_PRODUCT);
        }
        // check quantity
        Product orderedProduct = opOrderedProduct.get();
        if (!validOrderedQuantity(orderedProduct.getQuantity(), orderedQuantity)) {
            throw new InvalidDatabaseEntityException(OrderEntityValidationResult.PRODUCT_QUANTITY_EXCEEDED);
        }
        // create order instance
        SavableOrder newOrder = new SavableOrder(client.getId(), orderedProduct.getId(),
                orderedQuantity, orderedProduct.getUnitPrice(), LocalDateTime.now());
        DatabaseEntityValidationResult<Order> orderValidationResult =
                orderValidator.validate(newOrder.getObjectToSave());
        if (!orderValidationResult.isValid()) {
            throw new InvalidDatabaseEntityException(orderValidationResult);
        }
        // compute remaining quantity
        orderedProduct.setQuantity(orderedProduct.getQuantity() - orderedQuantity);
        DatabaseEntityValidationResult<Product> productValidationResult =
                productValidator.validate(orderedProduct);
        if (!productValidationResult.isValid()) {
            throw new InvalidDatabaseEntityException(productValidationResult);
        }
        // execute
        executeOrderCreation(orderedProduct, referredClient.get(), newOrder);
    }

    private void executeOrderCreation(Product orderedProduct,
                                      Client referredClient,
                                      SavableOrder newOrder) throws InvalidDatabaseEntityException, FailedOperationException {
        Optional<Order> opSavedOrder;
        try {
            ProductService.getInstance().edit(orderedProduct);
            opSavedOrder = orderDAO.insert(newOrder);
            if (opSavedOrder.isEmpty()) {
                throw new UnexpectedDatabaseException();
            }
            // create bill
            BillGenerator.createBill(referredClient, orderedProduct, opSavedOrder.get());
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            throw new FailedOperationException();
        }
    }

    private Optional<Client> getReferredClient(int referredId) {
        try {
            return ClientService.getInstance().findById(referredId);
        } catch (UnexpectedDatabaseException | FailedOperationException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private Optional<Product> getReferredProduct(int referredId) {
        try {
            return ProductService.getInstance().findById(referredId);
        } catch (UnexpectedDatabaseException | FailedOperationException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            return Optional.empty();
        }
    }

    private boolean validOrderedQuantity(int availableQuantity, int orderedQuantity) {
        return availableQuantity >= orderedQuantity;
    }
}
