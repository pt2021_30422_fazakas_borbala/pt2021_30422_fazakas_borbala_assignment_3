package com.bori.bll.Services;

import com.bori.bll.Validators.ProductValidator.ProductEntityValidator;
import com.bori.bll.Validators.ProductValidator.ProductEntityValidator;
import com.bori.dal.DAO.ProductDAO;
import com.bori.dal.DAO.ProductDAO;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Product;
import com.bori.model.Saved.Product;

/**
 * Defines a service for saving, updating, finding and deleting product data.
 * Implemented with the singleton pattern.
 */
public class ProductService extends AbstractService<Product, Integer> {
    private static ProductService instance;
    
    private ProductService() throws UnexpectedDatabaseException {
        super(new ProductDAO(), new ProductEntityValidator());
    }

    /**
     * Provides access to the single instance of ProductService.
     * @return the single instance of ProductService.
     * @throws UnexpectedDatabaseException if ProductService could not be constructed because no
     * connection to the database could be established.
     */
    public static ProductService getInstance() throws UnexpectedDatabaseException {
        if (instance == null) {
            instance = new ProductService();
        }
        return instance;
    }
}
