package com.bori.bll.Services;

import com.bori.bll.Validators.ClientValidator.ClientEntityValidator;
import com.bori.bll.Validators.DatabaseEntityValidator;
import com.bori.dal.DAO.ClientDAO;
import com.bori.dal.DAO.IDAO;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Client;

/**
 * Defines a service for saving, updating, finding and deleting client data.
 * Implemented with the singleton pattern.
 */
public class ClientService extends AbstractService<Client, Integer> {
    private static ClientService instance;

    private ClientService() throws UnexpectedDatabaseException {
        super(new ClientDAO(), new ClientEntityValidator());
    }

    /**
     * Provides access to the single instance of ClientService.
     * @return the single instance of ClientService.
     * @throws UnexpectedDatabaseException if ClientService could not be constructed because no
     * connection to the database could be established.
     */
    public static ClientService getInstance() throws UnexpectedDatabaseException {
        if (instance == null) {
            instance = new ClientService();
        }
        return instance;
    }
}
