package com.bori.bll.Services;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.bll.Util.PropertyChangeObservable;
import com.bori.dal.Entities.DatabaseEntity;
import com.bori.dal.Entities.SavableEntity;

import java.util.List;
import java.util.Optional;

/**
 * Defines a generic service interface for saving, updating, finding and deleting application data.
 * @param <T> defines the type of the application data.
 * @param <K> defines the type of T's id.
 */
public interface IService<T extends DatabaseEntity<K>, K> extends PropertyChangeObservable {
    /**
     * Searches for and returns all existing instances from T.
     * @return a list containing all instances of T.
     * @throws FailedOperationException if an unexpected operation occurred and the result could
     * not be obtained.
     */
    List<T> findAll() throws FailedOperationException;

    /**
     * Searches for and returns an instance of T with id equal to key.
     * @param key is the searched primary key.
     * @return an Optional containing the searched element, if it exists, otherwise an empty
     * optional.
     * @throws FailedOperationException if an unexpected operation occurred and the result could
     * not be obtained.
     */
    Optional<T> findById(K key) throws FailedOperationException;

    /**
     * Saves the data of a new entity.
     * @param entity is the new entity to save.
     * @throws FailedOperationException if an unexpected operation occurred and the entity could
     * not be inserted.
     * @throws InvalidDatabaseEntityException if the data of the entity is invalid.
     */
    void insert(SavableEntity<T> entity) throws FailedOperationException,
            InvalidDatabaseEntityException;

    /**
     * Deletes the data of the T instance with the id equal to key.
     * @param key is the id of the entity to delete.
     * @throws FailedOperationException if an unexpected operation occurred and the entity could
     * not be deleted.
     */
    void delete(K key) throws FailedOperationException;

    /**
     * Updates the data of the entity.
     * @param entity is the entity whose data is edited.
     * @throws FailedOperationException if an unexpected operation occurred and the entity could
     * not be updated.
     * @throws InvalidDatabaseEntityException if the data of the entity is invalid.
     */
    void edit(T entity) throws FailedOperationException, InvalidDatabaseEntityException;
}
