package com.bori.bll.Services;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.bll.Validators.DatabaseEntityValidationResult;
import com.bori.bll.Validators.DatabaseEntityValidator;
import com.bori.dal.DAO.IDAO;
import com.bori.dal.Entities.DatabaseEntity;
import com.bori.dal.Entities.SavableEntity;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Optional;

/**
 * Provides an implementation for {@link IService}.
 * @param <T> defines the type of the entity.
 * @param <K> defines the type of the entity's key.
 */
public abstract class AbstractService<T extends DatabaseEntity<K>, K> implements IService<T, K> {

    /**
     * Specifies the event types that may occur in the observable Service and observers can
     * listen to.
     */
    public enum Events {
        /** Event fired when a new entity is saved. */
        NEW_ENTITY,
        /** Event fired when an entity is deleted. */
        REMOVED_ENTITY,
        /** Event fired when the data of an entity is deleted. */
        EDITED_ENTITY
    }

    private final IDAO<T, K> DAO;
    private final DatabaseEntityValidator<T> validator;
    private final PropertyChangeSupport support = new PropertyChangeSupport(this);

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    protected AbstractService(IDAO<T, K> DAO, DatabaseEntityValidator<T> validator) {
        this.DAO = DAO;
        this.validator = validator;
    }

    /** {@inheritDoc} */
    @Override
    public List<T> findAll() throws FailedOperationException {
        try {
            return DAO.findAll();
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            throw new FailedOperationException();
        }
    }

    /** {@inheritDoc} */
    @Override
    public Optional<T> findById(K key) throws FailedOperationException {
        try {
            return DAO.findById(key);
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            throw new FailedOperationException();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void insert(SavableEntity<T> entity) throws FailedOperationException,
            InvalidDatabaseEntityException {
        DatabaseEntityValidationResult<T> validationResult = validator.validate(entity.getObjectToSave());
        if (validationResult.isValid()) {
            try {
                Optional<T> inserted = DAO.insert(entity);
                if (inserted.isPresent()) {
                    support.firePropertyChange(Events.NEW_ENTITY.toString(), null, inserted.get());
                } else {
                    throw new UnexpectedDatabaseException();
                }
            } catch (UnexpectedDatabaseException e) {
                LOGGER.error(Throwables.getStackTraceAsString(e));
                e.printStackTrace();
                throw new FailedOperationException();
            }
        } else {
            throw new InvalidDatabaseEntityException(validationResult);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void delete(K key) throws FailedOperationException {
        try {
            DAO.delete(key);
            support.firePropertyChange(Events.REMOVED_ENTITY.toString(), null, key);
            // object
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            throw new FailedOperationException();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void edit(T entity) throws FailedOperationException, InvalidDatabaseEntityException {
        DatabaseEntityValidationResult<T> validationResult = validator.validate(entity);
        if (validationResult.isValid()) {
            try {
                Optional<T> edited = DAO.edit(entity);
                if (edited.isPresent()) {
                    support.firePropertyChange(Events.EDITED_ENTITY.toString(), null, edited.get());
                } else {
                    throw new UnexpectedDatabaseException();
                }
            } catch (UnexpectedDatabaseException e) {
                LOGGER.error(Throwables.getStackTraceAsString(e));
                e.printStackTrace();
                throw new FailedOperationException();
            }
        } else {
            throw new InvalidDatabaseEntityException(validationResult);
        }
    }

    /** {@inheritDoc} */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    /** {@inheritDoc} */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }
}
