package com.bori.bll.Util;

import java.beans.PropertyChangeListener;

/**
 * Defines a generic interface for Observable objects (as specified in the Observer pattern.
 */
public interface PropertyChangeObservable {

    /**
     * Registers a new listener to the Observable object.
     * @param listener is the newly registered listener.
     */
    void addPropertyChangeListener(PropertyChangeListener listener);

    /**
     * Removes a listener from the Observable object.
     * @param listener is the removed listener.
     */
    void removePropertyChangeListener(PropertyChangeListener listener);
}
