package com.bori.bll.Util;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.dal.Entities.DatabaseEntity;
import com.bori.dal.Util.DatabaseEntityUtil;
import com.bori.model.Saved.Client;
import com.bori.model.Saved.Order;
import com.bori.model.Saved.Product;
import com.google.common.base.Throwables;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * BillGenerator is reposnible for creating a bill for a specific order, in a pdf file containing
 * data about the client, the product and the total sum to pay.
 */
public class BillGenerator {
    private static final Font TITLE_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 36,
            Font.BOLD);
    private static final Font SMALL_BOLD_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    private static final Font SMALL_FONT = new Font(Font.FontFamily.TIMES_ROMAN, 10);

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    private static final String FILE_PATH_FORMAT = "Bills/Bill-%s.pdf";
    private static final String LOGO_PATH = "src/main/resources/img/logo.png";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(
            "yyyy_MM_dd-HH_mm_ss");

    /**
     * Creates a pdf representing a bill for the order, sent by the client for the specified
     * product.
     * @param client is the client who sent the order.
     * @param product is the requested product.
     * @param order is the order for which the bill is created.
     */
    public static void createBill(Client client, Product product, Order order)  {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(String.format(FILE_PATH_FORMAT,
                    order.getTimeStamp().format(DATE_TIME_FORMATTER))));
            document.open();
            addMetaData(document);
            addContent(document, client, product, order);
            document.close();
        } catch (DocumentException | IOException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
        }
    }

    private static void addMetaData(Document document) {
        document.addTitle("Order Bill");
        document.addSubject("Order");
        document.addAuthor("Bori");
        document.addCreator("Bori");
    }

    private static void addContent(Document document, Client client, Product product, Order order)
            throws DocumentException, IOException {
        addPrefaceParagraph(document);
        addDataParagraph(document, client, product);
        addTotalParagraph(document, order);
    }

    private static void addPrefaceParagraph(Document document) throws DocumentException, IOException {
        Paragraph preface = new Paragraph();
        Image image = Image.getInstance(LOGO_PATH);
        image.setAlignment(Element.ALIGN_CENTER);
        image.scaleAbsolute(100, 100);
        preface.add(image);
        addEmptyLine(preface, 1);
        Paragraph billPara = new Paragraph("Bill", TITLE_FONT);
        billPara.setAlignment(Element.ALIGN_CENTER);
        preface.add(billPara);
        addEmptyLine(preface, 1);
        document.add(preface);
    }

    private static void addDataParagraph(Document document, Client client, Product product) throws DocumentException {
        Paragraph dataPara = new Paragraph();
        dataPara.add(new Paragraph("Customer Data", SMALL_BOLD_FONT));
        addEmptyLine(dataPara, 1);
        dataPara.setFont(SMALL_FONT);
        addTable(dataPara, Client.class, client);
        addEmptyLine(dataPara, 3);
        dataPara.add(new Paragraph("Product Data", SMALL_BOLD_FONT));
        addEmptyLine(dataPara, 1);
        dataPara.setFont(SMALL_FONT);
        addTable(dataPara, Product.class, product);
        document.add(dataPara);
    }

    private static void addTotalParagraph(Document document, Order order) throws DocumentException {
        Paragraph totalPara = new Paragraph();
        addEmptyLine(totalPara, 10);
        Paragraph quantityPara = new Paragraph("Ordered Quantity: " + order.getQuantity(),
                SMALL_BOLD_FONT);
        quantityPara.setAlignment(Element.ALIGN_RIGHT);
        totalPara.add(quantityPara);
        Paragraph pricePara = new Paragraph("Total Price: " + order.getUnitPrice().multiply(new BigDecimal(order.getQuantity())),
                SMALL_BOLD_FONT);
        pricePara.setAlignment(Element.ALIGN_RIGHT);
        totalPara.add(pricePara);
        addEmptyLine(totalPara, 1);
        Paragraph datePara =
                new Paragraph("Date: " + order.getTimeStamp().format(DATE_TIME_FORMATTER),
                        SMALL_FONT);
        datePara.setAlignment(Element.ALIGN_RIGHT);
        totalPara.add(datePara);
        document.add(totalPara);
    }

    private static <T extends DatabaseEntity> void addTable(Paragraph paragraph, Class<T> type,
                                                                 T object) throws DocumentException{
        List<Field> fieldList = DatabaseEntityUtil.getNonAutoGeneratedColumnFields(type);
        PdfPCell c1;
        PdfPTable table = new PdfPTable(2);
        for (Field field : fieldList) {
            if (!field.getName().equals("quantity")) { //todo
                c1 = new PdfPCell(Phrase.getInstance(DatabaseEntityUtil.getFieldDatabaseName(field)));
                c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(c1);
                field.setAccessible(true);
                try {
                    table.addCell(field.get(object).toString());
                } catch (IllegalAccessException e) {
                    table.addCell("Hidden");
                }
            }
        }

        paragraph.add(table);
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
