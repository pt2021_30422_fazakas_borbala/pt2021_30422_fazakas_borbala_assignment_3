package com.bori.presenter;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.bll.Services.ClientService;
import com.bori.bll.Services.IService;
import com.bori.bll.Services.ProductService;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Savable.SavableClient;
import com.bori.model.Savable.SavableProduct;
import com.bori.model.Saved.Client;
import com.bori.model.Saved.Product;
import com.bori.presenter.Util.AlertFactory;
import com.google.common.base.Throwables;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * ProductSpecificationController is the controller corresponding to product_specification_pane,
 * and it is responsible for taking user inputs and updating product data/creating a new product,
 */
public class ProductSpecificationController {
    /** Text field for the product's name. */
    @FXML
    public TextField nameTextField;

    /** Text field for the product's unit price. */
    @FXML
    public TextField unitPriceTextField;

    /** Text field for the product's stock quantity. */
    @FXML
    public TextField quantityTextField;

    /** Button for saving the new product/saving the changes to the existing product. */
    @FXML
    public Button saveButton;

    /**
     * When the pane is loaded, its controller must be initialized: either with a productToEdit,
     * if the data of an existing product is edited, or with no arguments, if a new product is to
     * be created. See {@link ProductSpecificationController#init(Product)} and
     * {@link ProductSpecificationController#init()}.
     */
    boolean initialized = false;

    /**
     * In case an existing product is edited, productToEdit specifies a reference to it.
     */
    private Product productToEdit;

    private IService<Product, Integer> service;

    private static final String EMPTY_STRING = "";

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Sets up the controller in case an existing Product product is to be edited.
     * @param product is the product to edit.
     */
    public void init(Product product) {
        if (initialized) {
            throw new IllegalStateException();
        }
        initialized = true;
        this.productToEdit = product;
        setupNumericInputs();
        fillInputs(product);
        try {
            service = ProductService.getInstance();
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert(e);
        }
        saveButton.setOnAction(actionEvent -> onEditProduct());
    }

    /**
     * Sets up the controller in case a new product is to be created.
     */
    public void init() {
        if (initialized) {
            throw new IllegalStateException();
        }
        initialized = true;
        setupNumericInputs();
        try {
            service = ProductService.getInstance();
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert(e);
        }
        saveButton.setOnAction(actionEvent -> onSaveNewProduct());
    }

    /**
     * Extracts the user input and updated the data of the product.
     * In case of success, a message is displayed and the window is closed.
     * In case of an error, an error message is displayed and the data is reset.
     */
    private void onEditProduct() {
        Product editedProduct = getEditedProduct();
        try {
            service.edit(editedProduct);
            AlertFactory.showSuccesfulOperationAlert();
            closeStage();
        } catch (FailedOperationException e) {
            AlertFactory.showErrorAlert(e);
            fillInputs(productToEdit); //reset data
        } catch (InvalidDatabaseEntityException e) {
            AlertFactory.showErrorAlert(e);
            fillInputs(productToEdit); //reset data
        }
    }

    /**
     * Extracts the user input and creates a new product.
     * In case of success, a message is displayed and the window is closed.
     * In case of an error, an error message is displayed and the data is reset.
     */
    private void onSaveNewProduct() {
        SavableProduct newProduct = getSavableProduct();
        try {
            service.insert(newProduct);
            AlertFactory.showSuccesfulOperationAlert();
            closeStage();
        } catch (FailedOperationException e) {
            AlertFactory.showErrorAlert(e);
            clearInputs();
        } catch (InvalidDatabaseEntityException e) {
            AlertFactory.showErrorAlert(e);
            clearInputs();
        }
    }

    private void closeStage() {
        Stage stage = (Stage) saveButton.getScene().getWindow();
        stage.close();
    }

    private SavableProduct getSavableProduct() {
        return new SavableProduct(nameTextField.getText(),
                new BigDecimal(unitPriceTextField.getText()),
                Integer.parseInt(quantityTextField.getText()));
    }

    private Product getEditedProduct() {
        return new Product(productToEdit.getId(),
                nameTextField.getText(),
                new BigDecimal(unitPriceTextField.getText()),
                Integer.parseInt(quantityTextField.getText()));
    }

    private void fillInputs(Product product) {
        nameTextField.setText(product.getName());
        quantityTextField.setText(String.valueOf(product.getQuantity()));
        unitPriceTextField.setText(String.valueOf(product.getUnitPrice()));
    }

    private void clearInputs() {
        nameTextField.setText(EMPTY_STRING);
        quantityTextField.setText("0");
        unitPriceTextField.setText("0.00");
    }

    private void setupNumericInputs() {
        quantityTextField.setTextFormatter(getIntegerFormatter());
        unitPriceTextField.setTextFormatter(getDoubleFormatter());
    }

    private TextFormatter<Integer> getIntegerFormatter() {
        return new TextFormatter<>(
                new IntegerStringConverter(),
                0,
                c -> Pattern.matches("\\d*", c.getText()) ? c : null);
    }

    private TextFormatter<Double> getDoubleFormatter() {
        Pattern validEditingState = Pattern.compile("-?(([1-9][0-9]*)|0)?(\\.[0-9]*)?");

        UnaryOperator<TextFormatter.Change> filter = c -> {
            String text = c.getControlNewText();
            if (validEditingState.matcher(text).matches()) {
                return c ;
            } else {
                return null ;
            }
        };

        StringConverter<Double> converter = new StringConverter<>() {
            @Override
            public Double fromString(String s) {
                if (s.isEmpty() || "-".equals(s) || ".".equals(s) || "-.".equals(s)) {
                    return 0.0;
                } else {
                    return Double.valueOf(s);
                }
            }

            @Override
            public String toString(Double d) {
                return d.toString();
            }
        };
        return new TextFormatter<>(converter, 0.0, filter);
    }
}
