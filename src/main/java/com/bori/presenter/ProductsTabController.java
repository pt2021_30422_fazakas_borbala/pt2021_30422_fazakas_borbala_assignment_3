package com.bori.presenter;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Services.ClientService;
import com.bori.bll.Services.ProductService;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Client;
import com.bori.model.Saved.Product;
import com.bori.presenter.Util.AlertFactory;
import com.google.common.base.Throwables;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

/**
 * ProductsTabController controls the products_pane, and its allows user to access editing, deleting
 * and creation functionalities for user.
 */
public class ProductsTabController {
    /** Table containing product data. */
    @FXML
    public TableView<Product> productsTable;

    /** Button for cerating a new client. */
    @FXML
    public Button newProductButton;

    private static final String DELETE_TEXT = "Delete";
    private static final String EDIT_TEXT = "Edit";

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Initialized the tab, sets up the tabe and the action buttons.
     * @throws UnexpectedDatabaseException if the connetcion to the service could not be
     * established.
     */
    @FXML
    public void initialize() throws UnexpectedDatabaseException {
        new TableController<>(Product.class,
                ProductService.getInstance(),
                productsTable);
        productsTable.setPlaceholder(new Label("No Products to display"));
        setupEditButtonColumn();
        setupDeleteButtonColumn();
        newProductButton.setOnAction(actionEvent -> onNewProductButtonClicked());
    }

    /**
     * Loads the pane for product creation.
     */
    private void onNewProductButtonClicked() {
        FXMLLoader productSpecPaneLoader = new FXMLLoader(getClass().getResource(
                "/product_specification_pane.fxml"));
        AnchorPane productSpecPane;
        try {
            productSpecPane = productSpecPaneLoader.load();
            ProductSpecificationController controller = productSpecPaneLoader.getController();
            controller.init();
            Stage stage = new Stage();
            stage.setTitle("Edit product data");
            stage.initOwner(this.productsTable.getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(productSpecPane));
            stage.show();
        } catch (IOException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert();
        }
    }

    /**
     * Adds an edit button for each row of the table.
     */
    private void setupEditButtonColumn() {
        TableColumn<Product, Void> actionCol = new TableColumn<>(EDIT_TEXT);
        alignCenter(actionCol);

        Callback<TableColumn<Product, Void>, TableCell<Product, Void>> cellFactory
                = new Callback<>() {
            @Override
            public TableCell<Product, Void> call(final TableColumn<Product, Void> param) {
                final TableCell<Product, Void> cell = new TableCell<>() {

                    private final Button btn = new Button(EDIT_TEXT);
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Product product = getTableView().getItems().get(getIndex());
                            onEditButtonClicked(product);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        actionCol.setCellFactory(cellFactory);
        productsTable.getColumns().add(actionCol);
    }

    /**
     * Adds a delete button for each row of the table.
     */
    private void setupDeleteButtonColumn() {
        TableColumn<Product, Void> actionCol = new TableColumn<>(DELETE_TEXT);
        alignCenter(actionCol);

        Callback<TableColumn<Product, Void>, TableCell<Product, Void>> cellFactory
                = new Callback<>() {
            @Override
            public TableCell<Product, Void> call(final TableColumn<Product, Void> param) {
                final TableCell<Product, Void> cell = new TableCell<>() {

                    private final Button btn = new Button(DELETE_TEXT);
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Product productToDelete = getTableView().getItems().get(getIndex());
                            onDelete(productToDelete);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        actionCol.setCellFactory(cellFactory);
        productsTable.getColumns().add(actionCol);
    }

    /**
     * Opens the pane for editing the selected product.
     * @param product is the product to edit.
     * @throws IOException if the xml file for the pane cannot be loaded.
     */
    private void onEditButtonClicked(Product product) {
        FXMLLoader productSpecPaneLoader = new FXMLLoader(getClass().getResource(
                "/product_specification_pane.fxml"));
        AnchorPane productSpecPane;
        try {
            productSpecPane = productSpecPaneLoader.load();
            ProductSpecificationController controller = productSpecPaneLoader.getController();
            controller.init(product);
            Stage stage = new Stage();
            stage.setTitle("Edit product data");
            stage.initOwner(this.productsTable.getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(productSpecPane));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace(); //todo
        }
    }

    /**
     * Allows the user to delete the selected product after confirming this intention in an alert
     * window.
     * @param entity is the product to delete.
     */
    protected void onDelete(Product entity) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Deletion");
        alert.setHeaderText("Are you sure that you want to delete this entity? ");
        alert.setContentText(entity.toString());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            try {
                ProductService.getInstance().delete(entity.getId());
                AlertFactory.showSuccesfulOperationAlert();
            } catch (FailedOperationException e) {
                AlertFactory.showErrorAlert(e);
            } catch (UnexpectedDatabaseException e) {
                AlertFactory.showErrorAlert(e);
            }
        }
    }

    private void alignCenter(TableColumn tableColumn) {
        tableColumn.setStyle( "-fx-alignment: CENTER;");
    }
}
