package com.bori.presenter;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.bll.Services.ClientService;
import com.bori.bll.Services.IService;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Savable.SavableClient;
import com.bori.model.Saved.Client;
import com.bori.presenter.Util.AlertFactory;
import com.google.common.base.Throwables;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.LongStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

/**
 * ClientSpecificationController is the controller corresponding to client_specification_pane,
 * and it is responsible for taking user inputs and updating user data/creating a new user,
 */
public class ClientSpecificationController {
    /** Text field for specifying the username. */
    @FXML
    public TextField userNameTextField;

    /** Text field for specifying the user's address. */
    @FXML
    public TextField addressTextField;

    /** Text field for specifying the user's email address. */
    @FXML
    public TextField emailAddressTextField;

    /** Text field for specifying the user's phone number. */
    @FXML
    public TextField phoneNumberTextField;

    /** Text field for specifying the user's first name. */
    @FXML
    public TextField firstNameTextField;

    /** Text field for specifying the user's last name. */
    @FXML
    public TextField lastNameTextField;

    /** Button for saving the new user/saving the changes to the existing user */
    @FXML
    public Button saveButton;

    /**
     * When the pane is loaded, its controller must be initialized: either with a clientToEdit,
     * if the data of an existing client is edited, or with no arguments, if a new client is to
     * be created. See {@link ClientSpecificationController#init(Client)} and
     * {@link ClientSpecificationController#init()}.
     */
    private boolean initialized = false;

    /**
     * In case an existing client is edited, clientToEdit specifies a reference to it.
     */
    private Client clientToEdit;

    private IService<Client, Integer> service;

    private static final String EMPTY_STRING = "";

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Sets up the controller in case an existing client is to be edited.
     * @param client is the client to edit.
     */
    public void init(Client client) {
        if (initialized) {
            throw new IllegalStateException();
        }
        initialized = true;
        this.clientToEdit = client;
        setupNumericInputs();
        fillInputs(client);
        try {
            service = ClientService.getInstance();
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert(e);
        }
        saveButton.setOnAction(actionEvent -> onEditClient());
    }

    /**
     * Sets up the controller in case a new client is to be created.
     */
    public void init() {
        if (initialized) {
            throw new IllegalStateException();
        }
        initialized = true;
        setupNumericInputs();
        try {
            service = ClientService.getInstance();
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert(e);
        }
        saveButton.setOnAction(actionEvent -> onSaveNewClient());
    }

    /**
     * Extracts the user input and updated the data of the client.
     * In case of success, a message is displayed and the window is closed.
     * In case of an error, an error message is displayed and the data is reset.
     */
    private void onEditClient() {
        Client toEdit = getEditedClient();
        try {
            service.edit(toEdit);
            AlertFactory.showSuccesfulOperationAlert();
            closeStage();
        } catch (FailedOperationException e) {
            AlertFactory.showErrorAlert(e);
            fillInputs(clientToEdit); //reset data
        } catch (InvalidDatabaseEntityException e) {
            AlertFactory.showErrorAlert(e);
            fillInputs(clientToEdit); //reset data
        }
    }

    /**
     * Extracts the user input and creates a new client.
     * In case of success, a message is displayed and the window is closed.
     * In case of an error, an error message is displayed and the data is reset.
     */
    private void onSaveNewClient() {
        SavableClient toSave = getSavableClient();
        try {
            service.insert(toSave);
            AlertFactory.showSuccesfulOperationAlert();
            closeStage();
        } catch (FailedOperationException e) {
            AlertFactory.showErrorAlert(e);
            clearInputs(); //reset data
        } catch (InvalidDatabaseEntityException e) {
            AlertFactory.showErrorAlert(e);
            clearInputs(); //reset data
        }
    }

    private void closeStage() {
        Stage stage = (Stage) saveButton.getScene().getWindow();
        stage.close();
    }

    private SavableClient getSavableClient() {
        return new SavableClient(userNameTextField.getText(),
                firstNameTextField.getText(),
                lastNameTextField.getText(),
                addressTextField.getText(),
                phoneNumberTextField.getText(),
                emailAddressTextField.getText());
    }

    private Client getEditedClient() {
        return new Client(clientToEdit.getId(),
                userNameTextField.getText(),
                firstNameTextField.getText(),
                lastNameTextField.getText(),
                addressTextField.getText(),
                phoneNumberTextField.getText(),
                emailAddressTextField.getText());
    }

    private void fillInputs(Client client) {
        userNameTextField.setText(client.getUserName());
        firstNameTextField.setText(client.getFirstName());
        lastNameTextField.setText(client.getLastName());
        addressTextField.setText(client.getAddress());
        phoneNumberTextField.setText(client.getPhoneNumber());
        emailAddressTextField.setText(client.getEmailAddress());
    }

    private void clearInputs() {
        userNameTextField.setText(EMPTY_STRING);
        firstNameTextField.setText(EMPTY_STRING);
        lastNameTextField.setText(EMPTY_STRING);
        addressTextField.setText(EMPTY_STRING);
        phoneNumberTextField.setText(EMPTY_STRING);
        emailAddressTextField.setText(EMPTY_STRING);
    }

    private void setupNumericInputs() {
        phoneNumberTextField.setTextFormatter(getLongFormatter());
    }

    private TextFormatter<String> getLongFormatter() {
        return new TextFormatter<>(
                TextFormatter.IDENTITY_STRING_CONVERTER,
                "0740000000",
                c -> Pattern.matches("\\d*", c.getText()) ? c : null);
    }
}
