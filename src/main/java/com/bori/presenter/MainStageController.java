package com.bori.presenter;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * MainStageController is the controller corresponding to main_stage and it sets up the main
 * window of the application.
 */
public class MainStageController {
    @FXML
    public Tab clientsTab;

    @FXML
    public Tab productsTab;

    @FXML
    public Tab ordersTab;

    /**
     * Initialization method called when the frame is loaded. It sets up the tabs.
     * @throws IOException if the tabs' fxml files cannot be accessed.
     */
    @FXML
    public void initialize() throws IOException {
        setupClientsTab();
        setupProductsTab();
        setupOrdersTab();
    }

    /** Loads clientsTab's data. */
    private void setupClientsTab() throws IOException {
        FXMLLoader clientsPaneLoader = new FXMLLoader(getClass().getResource(
                "/clients_pane.fxml"));
        AnchorPane clientsPane = clientsPaneLoader.load();
        clientsTab.setContent(clientsPane);
    }

    /** Loads productsTab's data. */
    private void setupProductsTab() throws IOException {
        FXMLLoader productsPaneLoader = new FXMLLoader(getClass().getResource(
                "/products_pane.fxml"));
        AnchorPane productsPane = productsPaneLoader.load();
        productsTab.setContent(productsPane);
    }

    /** Loads ordersTab's data. */
    private void setupOrdersTab() throws IOException {
        FXMLLoader ordersPaneLoader = new FXMLLoader(getClass().getResource(
                "/orders_pane.fxml"));
        AnchorPane ordersPane = ordersPaneLoader.load();
        ordersTab.setContent(ordersPane);
    }
}
