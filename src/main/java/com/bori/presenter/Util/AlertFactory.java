package com.bori.presenter.Util;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import javafx.scene.control.Alert;

/**
 * Generates alerts dialog for the user explaining the error that occurres and possibly suggesting
 * solutions.
 */
public class AlertFactory {
    /**
     * Generates an alert dialog for a failed operation.
     * @param e is the exception that occurred and about which the user must be informed.
     */
    public static void showErrorAlert(FailedOperationException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Unexpected Error");
        alert.setHeaderText("Unfortunately this operation could not be executed. Please retry or " +
                "contact an administrator");
        alert.show();
    }

    /**
     * Generates an alert dialog for an unexpected database exception.
     * @param e is the exception that occurred and about which the user must be informed.
     */
    public static void showErrorAlert(UnexpectedDatabaseException e) {
        showErrorAlert();
    }

    /**
     * Generates an alert dialog for a generic error.
     */
    public static void showErrorAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Unexpected Error");
        alert.setHeaderText("Please contact an administrator.");
        alert.show();
    }

    /**
     * Generates an alert dialog for an invalid database entity data exception.
     * @param e is the exception that occurred and about which the user must be informed.
     */
    public static void showErrorAlert(InvalidDatabaseEntityException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Data");
        alert.setHeaderText("Seems like some data that you provided is invalid. Please correct " +
                "the mistakes");
        alert.setContentText(e.getMessage());
        alert.show();
    }

    /**
     * Generates an alert dialog confirming that an operation was successfully executed.
     */
    public static void showSuccesfulOperationAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Changes successfully saved");
        alert.show();
    }
}
