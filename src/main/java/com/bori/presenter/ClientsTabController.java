package com.bori.presenter;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Services.ClientService;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Client;
import com.bori.presenter.Util.AlertFactory;
import com.google.common.base.Throwables;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

/**
 * ClientsTabController controls the clients_pane, and its allows user to access editing, deleting
 * and creation functionalities for user.
 */
public class ClientsTabController {
    /** Table containing client data. */
    @FXML
    public TableView<Client> clientsTable;

    /** Button for cerating a new client. */
    @FXML
    public Button newClientButton;

    private static final String DELETE_TEXT = "Delete";
    private static final String EDIT_TEXT = "Edit";

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Initialized the tab, sets up the tabe and the action buttons.
     * @throws UnexpectedDatabaseException if the connetcion to the service could not be
     * established.
     */
    @FXML
    public void initialize() throws UnexpectedDatabaseException {
        TableController<Client, Integer> tableController = new TableController<>(Client.class, ClientService.getInstance(),
                clientsTable);
        clientsTable.setPlaceholder(new Label("No clients to display"));
        setupEditButtonColumn();
        setupDeleteButtonColumn();
        newClientButton.setOnAction(actionEvent -> onNewClientButtonClicked());
    }

    /**
     * Loads the pane for client creation.
     */
    private void onNewClientButtonClicked() {
        FXMLLoader clientSpecPaneLoader = new FXMLLoader(getClass().getResource(
                "/client_specification_pane.fxml"));
        AnchorPane clientSpecPane = null;
        try {
            clientSpecPane = clientSpecPaneLoader.load();
            ClientSpecificationController controller = clientSpecPaneLoader.getController();
            controller.init();
            Stage stage = new Stage();
            stage.setTitle("New Client");
            stage.initOwner(this.clientsTable.getScene().getWindow());
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(clientSpecPane));
            stage.show();
        } catch (IOException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert();
        }
    }

    /**
     * Adds an edit button for each row of the table.
     */
    private void setupEditButtonColumn() {
        TableColumn<Client, Void> actionCol = new TableColumn<>(EDIT_TEXT);
        alignCenter(actionCol);

        Callback<TableColumn<Client, Void>, TableCell<Client, Void>> cellFactory
                = new Callback<>() {
                    @Override
                    public TableCell<Client, Void> call(final TableColumn<Client, Void> param) {
                        final TableCell<Client, Void> cell = new TableCell<>() {

                            private final Button btn = new Button(EDIT_TEXT);
                            {
                                btn.setOnAction((ActionEvent event) -> {
                                    Client client = getTableView().getItems().get(getIndex());
                                    try {
                                        onEditButtonClicked(client);
                                    } catch (IOException e) {
                                        e.printStackTrace(); //todo
                                    }
                                });
                            }

                            @Override
                            public void updateItem(Void item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                } else {
                                    setGraphic(btn);
                                }
                            }
                        };
                        return cell;
                    }
                };
        actionCol.setCellFactory(cellFactory);
        clientsTable.getColumns().add(actionCol);
    }

    /**
     * Adds a delete button for each row of the table.
     */
    private void setupDeleteButtonColumn() {
        TableColumn<Client, Void> actionCol = new TableColumn<>(DELETE_TEXT);
        alignCenter(actionCol);

        Callback<TableColumn<Client, Void>, TableCell<Client, Void>> cellFactory
                = new Callback<>() {
            @Override
            public TableCell<Client, Void> call(final TableColumn<Client, Void> param) {
                final TableCell<Client, Void> cell = new TableCell<>() {

                    private final Button btn = new Button(DELETE_TEXT);
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Client clientToDelete = getTableView().getItems().get(getIndex());
                            onDelete(clientToDelete);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        actionCol.setCellFactory(cellFactory);
        clientsTable.getColumns().add(actionCol);
    }

    /**
     * Opens the pane for editing the selected client.
     * @param client is the client to edit.
     * @throws IOException if the xml file for the pane cannot be loaded.
     */
    private void onEditButtonClicked(Client client) throws IOException {
        FXMLLoader clientSpecPaneLoader = new FXMLLoader(getClass().getResource(
                "/client_specification_pane.fxml"));
        AnchorPane clientSpecPane = clientSpecPaneLoader.load();
        ClientSpecificationController controller = clientSpecPaneLoader.getController();
        controller.init(client);
        Stage stage = new Stage();
        stage.setTitle("Edit Client data");
        stage.initOwner(this.clientsTable.getScene().getWindow());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(clientSpecPane));
        stage.show();
    }

    /**
     * Allows the user to delete the selected client after confirming this intention in an alert
     * window.
     * @param entity is the client to delete.
     */
    private void onDelete(Client entity) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Deletion");
        alert.setHeaderText("Are you sure that you want to delete this entity? ");
        alert.setContentText(entity.toString());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            try {
                ClientService.getInstance().delete(entity.getId());
                AlertFactory.showSuccesfulOperationAlert();
            } catch (FailedOperationException e) {
                AlertFactory.showErrorAlert(e);
            } catch (UnexpectedDatabaseException e) {
                e.printStackTrace();
            }
        }
    }

    private void alignCenter(TableColumn tableColumn) {
        tableColumn.setStyle( "-fx-alignment: CENTER;");
    }
}
