package com.bori.presenter;

import com.bori.bll.Exceptions.FailedOperationException;
import com.bori.bll.Exceptions.InvalidDatabaseEntityException;
import com.bori.bll.Services.ClientService;
import com.bori.bll.Services.OrderService;
import com.bori.bll.Services.ProductService;
import com.bori.bll.Validators.OrderValidator.OrderEntityValidationResult;
import com.bori.dal.Exceptions.UnexpectedDatabaseException;
import com.bori.model.Saved.Client;
import com.bori.model.Saved.Order;
import com.bori.model.Saved.Product;
import com.bori.presenter.Util.AlertFactory;
import com.google.common.base.Throwables;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.converter.IntegerStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.Pattern;

/**
 * OrdersTabController controls the orders_pane, and its is responsible for collecting user
 * input and creating new orders.
 */
public class OrdersTabController {
    /** Table containing product data. */
    @FXML
    public TableView<Product> productsTable;

    /** Table containing client data. */
    @FXML
    public TableView<Client> clientsTable;

    /** Text field for specifying the quantity of the order. */
    @FXML
    public TextField quantityTextField;

    /** Label showing the name of the selected client/a missing client message. */
    @FXML
    public Label selectedClientLabel;

    /** Label showing the name of the selected product/a missing product message. */
    @FXML
    public Label selectedProductLabel;

    /** Button for submitting a new order. */
    @FXML
    public Button submitBtn;

    /**
     * The client selected by the user for the new order. Must not be null when submitting the
     * order.
     */
    private Client selectedClient;

    /**
     * The product selected by the user for the new order. Must not be null when submitting the
     * order.
     */
    private Product selectedProduct;

    private OrderService orderService;

    private static final Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    /**
     * Method called when the pane is loaded. It establishes connections to the business logic,
     * sets up the table controllers and listeners for user actions.
     */
    @FXML
    public void initialize() {
        try {
            new TableController<>(Product.class, ProductService.getInstance(), productsTable);
            new TableController<>(Client.class, ClientService.getInstance(), clientsTable,
                    List.of(Client.class.getDeclaredField("userName")));
            orderService = OrderService.getInstance();
        } catch (UnexpectedDatabaseException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert(e);
        } catch (NoSuchFieldException e) {
            LOGGER.error(Throwables.getStackTraceAsString(e));
            e.printStackTrace();
            AlertFactory.showErrorAlert();
        }
        setupNumericInputs();
        expectClientSelection();
        expectProductSelection();
        submitBtn.setOnAction(actionEvent -> onSubmitOrder());
    }

    /**
     * Submits a new order, on condition that a client and a product has been selected.
     * Shows a message in case of successful order, and an error message containing information
     * about an error if an error occurred.
     */
    private void onSubmitOrder() {
        if (selectedClient == null) {
            AlertFactory.showErrorAlert(new InvalidDatabaseEntityException(OrderEntityValidationResult.MISSING_REFERRED_CLIENT));
        } else if (selectedProduct == null) {
            AlertFactory.showErrorAlert(new InvalidDatabaseEntityException(OrderEntityValidationResult.MISSING_REFERRED_PRODUCT));
        } else {
            int quantity = Integer.parseInt(quantityTextField.getText());
            try {
                orderService.createOrder(selectedClient, selectedProduct, quantity);
                AlertFactory.showSuccesfulOperationAlert();
            } catch (InvalidDatabaseEntityException e) {
                AlertFactory.showErrorAlert(e);
            } catch (FailedOperationException e) {
                AlertFactory.showErrorAlert(e);
            }
        }
    }

    /** Listens to changes in the selected client. */
    private void expectClientSelection() {
        clientsTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        clientsTable.getSelectionModel().setCellSelectionEnabled(false);
        handleSelectedClient(selectedClient);
        clientsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            //Check whether item is selected and set value of selected item to Label
            selectedClient = newValue;
            handleSelectedClient(selectedClient);
        });
    }

    private void handleSelectedClient(Client client) {
        if (client == null) {
            selectedClientLabel.setText("Missing client!");
        } else {
            selectedClientLabel.setText("Selected: " + client.getUserName());
        }
    }

    /** Listens to changes in the selected product. */
    private void expectProductSelection() {
        productsTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        productsTable.getSelectionModel().setCellSelectionEnabled(false);
        handleSelectedProduct(null);
        productsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            //Check whether item is selected and set value of selected item to Label
            selectedProduct = newValue;
            handleSelectedProduct(selectedProduct);
        });
    }

    private void handleSelectedProduct(Product product) {
        if (product == null) {
            selectedProductLabel.setText("Missing product!");
        } else {
            selectedProductLabel.setText("Selected: " + product.getName());
        }
    }


    private void setupNumericInputs() {
        quantityTextField.setTextFormatter(getIntegerFormatter());
    }

    private TextFormatter<Integer> getIntegerFormatter() {
        return new TextFormatter<>(
                new IntegerStringConverter(),
                1,
                c -> Pattern.matches("\\d*", c.getText()) ? c : null );
    }
}
