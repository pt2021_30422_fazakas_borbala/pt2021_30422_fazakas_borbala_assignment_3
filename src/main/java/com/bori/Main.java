package com.bori;

import com.bori.bll.Services.OrderService;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Startes class of the application, which sets up the main stage.
 */
public class Main extends Application {
    private AnchorPane root;
    private Scene scene;

    /** Method called when the application is started. */
    @Override
    public void start(Stage primaryStage) throws Exception {
        loadRoot();

        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.setTitle("Warehouse");
        primaryStage.show();
    }

    private void  loadRoot() throws IOException {
        FXMLLoader mainStageLoader = new FXMLLoader(getClass().getResource("/main_stage.fxml"));
        root = mainStageLoader.load();
        root.setPadding(new Insets(10, 10, 10, 10));
    }

}
