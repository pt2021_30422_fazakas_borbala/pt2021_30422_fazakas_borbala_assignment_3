package com.bori.model.Savable;

import com.bori.dal.Entities.SavableEntity;
import com.bori.model.Saved.Order;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Represents a new order of the warehouse, whose data is not saved in the database yet.
 */
public class SavableOrder implements SavableEntity<Order> {
    /**
     * The responsibilities are delegated to an Order instance, but no access to the id is given.
     */
    private final Order order;

    public SavableOrder(int clientId, int productId, int quantity, BigDecimal unitPrice,
                        LocalDateTime timestamp) {
        this.order = new Order(-1, clientId, productId, quantity, unitPrice, timestamp);
    }

    public Optional<Integer> getClientId() {
        return order.getClientId();
    }

    public Optional<Integer> getProductId() {
        return order.getProductId();
    }

    public int getQuantity() {
        return order.getQuantity();
    }

    public BigDecimal getUnitPrice() {
        return order.getUnitPrice();
    }

    public LocalDateTime getTimeStamp() {
        return order.getTimeStamp();
    }

    /** {@inheritDoc} */
    @Override
    public Order getObjectToSave() {
        return order;
    }
}
