package com.bori.model.Savable;

import com.bori.dal.Entities.SavableEntity;
import com.bori.model.Saved.Product;

import java.math.BigDecimal;

/**
 * Represents a new product of the warehouse, whose data is not saved in the database yet.
 */
public class SavableProduct implements SavableEntity<Product> {
    /**
     * The responsibilities are delegated to a Product instance, but no access to the id is given.
     */
    private final Product product;

    public SavableProduct(String name, BigDecimal unitPrice, int quantity) {
        this.product = new Product(-1, name, unitPrice, quantity);
    }

    public String getName() {
        return product.getName();
    }

    public BigDecimal getUnitPrice() {
        return product.getUnitPrice();
    }

    public int getQuantity() {
        return product.getQuantity();
    }

    public void setName(String name) {
        product.setName(name);
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        product.setUnitPrice(unitPrice);
    }

    public void setQuantity(int quantity) {
        product.setQuantity(quantity);
    }

    /** {@inheritDoc} */
    @Override
    public Product getObjectToSave() {
        return product;
    }
}
