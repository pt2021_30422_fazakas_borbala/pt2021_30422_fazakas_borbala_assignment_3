package com.bori.model.Savable;

import com.bori.dal.Entities.SavableEntity;
import com.bori.model.Saved.Client;

/**
 * Represents a new client of the warehouse, whose data is not saved in the database yet.
 */
public class SavableClient implements SavableEntity<Client> {
    /**
     * The responsibilities are delegated to a Client instance, but no access to the id is given.
     */
    private final Client client;

    public SavableClient(String userName, String firstName, String lastName, String address,
                   String phoneNumber, String emailAddress) {
        this.client = new Client(-1, userName, firstName, lastName, address, phoneNumber,
                emailAddress);
    }

    public String getUserName() {
        return client.getUserName();
    }

    public void setUserName(String userName) {
        client.setUserName(userName);
    }

    public String getFirstName() {
        return client.getFirstName();
    }

    public void setFirstName(String firstName) {
        client.setFirstName(firstName);
    }

    public String getLastName() {
        return client.getLastName();
    }

    public void setLastName(String lastName) {
        client.setLastName(lastName);
    }

    public String getAddress() {
        return client.getAddress();
    }

    public void setAddress(String address) {
        client.setAddress(address);
    }

    public String getPhoneNumber() {
        return client.getPhoneNumber();
    }

    public void setPhoneNumber(String phoneNumber) {
        client.setPhoneNumber(phoneNumber);
    }

    public String getEmailAddress() {
        return client.getEmailAddress();
    }

    public void setEmailAddress(String emailAddress) {
        client.setEmailAddress(emailAddress);
    }

    /** {@inheritDoc} */
    @Override
    public Client getObjectToSave() {
        return client;
    }
}
